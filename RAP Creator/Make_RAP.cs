﻿using System;
using System.IO;
using System.Linq;

namespace RAP_Creator
{
    class Make_RAP
    {
        static public void CreateRAP(string path1, string cid, string rapValueStr, string path2 = null)
        {
            byte[] rapValue = Misc_Utils.HexStringToByteArray(rapValueStr);

            if (!String.IsNullOrEmpty(path1))
            {
                if (File.Exists(path1 + @"\" + cid + ".rap"))
                    File.SetAttributes(path1 + @"\" + cid + ".rap", FileAttributes.Normal);

                using (FileStream stream = new FileStream(path1 + @"\" + cid + ".rap", FileMode.Create, FileAccess.Write))
                using (BinaryWriter rapFile = new BinaryWriter(stream))
                    rapFile.Write(rapValue);
            }
            if (!String.IsNullOrEmpty(path2))
            {
                if (File.Exists(path2 + @"\" + cid + ".rap"))
                    File.SetAttributes(path2 + @"\" + cid + ".rap", FileAttributes.Normal);

                using (FileStream stream = new FileStream(path2 + @"\" + cid + ".rap", FileMode.Create, FileAccess.Write))
                using (BinaryWriter rapFile = new BinaryWriter(stream))
                    rapFile.Write(rapValue);
            }
        }
    }
}
