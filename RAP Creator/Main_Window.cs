﻿using STA.Settings;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace RAP_Creator
{
    // Declare that we'll have an "About..." entry in the system menu
    public partial class mWindow : About_System_Menu
    {
        // Create the INI object
        INIFile configFile = new INIFile("config.ini", false, true);

        private const string RAP_REGEX = @"[A-F\d]{32}";
        private const string CID_REGEX = @"[A-Z]{2}\d{4}\-[A-Z]{4}\d{5}_\d{2}\-.{16}";

        public mWindow()
        {
            if (!File.Exists("no autoupdate"))
            {
                Autoupdate autoupdate = new Autoupdate();
                autoupdate.Owner = this;
            }

            if (!File.Exists("config.ini"))
                using (StreamWriter cfgFile = File.CreateText("config.ini")) {}
            
            InitializeComponent();
            
            // Grab CID and RAP from the clipboard
            string entry = Clipboard.GetText();
            entry = entry.Replace(" ", "");

            string cid = Regex.Match(entry, CID_REGEX).Value;

            string rapValue = Regex.Match(entry, RAP_REGEX).Value;

            // Paste the CID and RAP into the textbox, but only if they were found/matched
            if (rapValue.Length == 32 && cid.Length == 36)
            {
                // Casting \n here causes a white square to show for catalinnc - Environment.NewLine will use the proper newline feed depending on the system (\r\n for Windows)
                rapEntryBox.Text = cid + Environment.NewLine;
                rapEntryBox.Text += rapValue;
            }

            // Check for the folder paths to save RAPs in
            rapPath1.Text = configFile.GetValue("Main", "Path1", "");
            if (!Directory.Exists(rapPath1.Text))
                rapPath1.Text = "";
            else
                // If the folder exists, enable the create button...
                createBtn.Enabled = true;

            rapPath2.Text = configFile.GetValue("Main", "Path2", "");
            if (!Directory.Exists(rapPath2.Text))
                rapPath2.Text = "";
            else
                createBtn.Enabled = true;
        }
        

        private void rapPath1Btn_Click(object sender, EventArgs e)
        {
            Folder_Select_Dialog path = new Folder_Select_Dialog();

            path.Title = "Select the first folder where you would like to place newly-created RAPs.";
            path.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (path.ShowDialog())
            {
                rapPath1.Text = path.FolderName;
                configFile.SetValue("Main", "Path1", path.FolderName);
                createBtn.Enabled = true;
            }
            else
            {
                rapPath1.Text = String.Empty;
                configFile.SetValue("Main", "Path1", String.Empty);
                if (rapPath2.Text == String.Empty)
                    createBtn.Enabled = false;
            }
        }


        private void rapPath2Btn_Click(object sender, EventArgs e)
        {
            Folder_Select_Dialog path = new Folder_Select_Dialog();

            path.Title = "Select the second folder where you would like to place newly-created RAPs.";
            path.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (path.ShowDialog())
            {
                rapPath2.Text = path.FolderName;
                configFile.SetValue("Main", "Path2", path.FolderName);
                createBtn.Enabled = true;
            }
            else
            {
                rapPath2.Text = String.Empty;
                configFile.SetValue("Main", "Path2", String.Empty);
                if (rapPath1.Text == String.Empty)
                    createBtn.Enabled = false;
            }
        }


        private void clearBtn_Click(object sender, EventArgs e)
        {
            // Delete the contents of the text box and give it focus
            rapEntryBox.Clear();
            rapEntryBox.Focus();
        }


        private void createBtn_Click(object sender, EventArgs e)
        {
            string entry = rapEntryBox.Text.Replace(" ", "");

            if (Regex.IsMatch(entry, CID_REGEX) && Regex.IsMatch(entry, RAP_REGEX))
            {
                string cid = Regex.Match(entry, CID_REGEX).Value;
                string rap = Regex.Match(entry, RAP_REGEX).Value;

                Make_RAP.CreateRAP(rapPath1.Text, cid, rap, rapPath2.Text);

                MessageBox.Show("Done!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
                MessageBox.Show("Invalid input!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        
        // Prevent the text box from automatically selecting all text
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.rapEntryBox.SelectionStart = this.rapEntryBox.Text.Length;
            this.rapEntryBox.DeselectAll();
        }
    }
}
