﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace RAP_Creator
{
    class MakeRAP
    {
        private const string RAP_REGEX = @"[A-F\d]{32}";
        private const string CID_REGEX = @"[A-Z]{2}\d{4}\-[A-Z]{4}\d{5}_\d{2}\-.{16}";
        static public bool CreateRAP(string path1, string contents, string path2 = null)
        {
            contents = contents.Replace(" ", "").ToUpper();

            string cid = Regex.Match(contents, CID_REGEX).Value;

            var rapValueStr = Regex.Match(contents, RAP_REGEX).Value;
            byte[] rapValue = StringToByteArray(rapValueStr);

            if (rapValue.Length == 16 && cid.Length == 36)
            {
                if (!String.IsNullOrEmpty(path1))
                {
                    if (File.Exists(path1 + @"\" + cid + ".rap"))
                        File.SetAttributes(path1 + @"\" + cid + ".rap", FileAttributes.Normal);

                    using (FileStream stream = new FileStream(path1 + @"\" + cid + ".rap", FileMode.Create, FileAccess.Write))
                    using (BinaryWriter rapFile = new BinaryWriter(stream))
                        rapFile.Write(rapValue);
                }
                if (!String.IsNullOrEmpty(path2))
                {
                    if (File.Exists(path2 + @"\" + cid + ".rap"))
                        File.SetAttributes(path2 + @"\" + cid + ".rap", FileAttributes.Normal);

                    using (FileStream stream = new FileStream(path2 + @"\" + cid + ".rap", FileMode.Create, FileAccess.Write))
                    using (BinaryWriter rapFile = new BinaryWriter(stream))
                        rapFile.Write(rapValue);
                }
                return true;
            }
            else
                return false;
        }

        private static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
