﻿namespace RAP_Creator
{
    partial class mWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mWindow));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rapPath2Btn = new System.Windows.Forms.Button();
            this.rapPath1Btn = new System.Windows.Forms.Button();
            this.rapPath2 = new System.Windows.Forms.TextBox();
            this.rapPath1 = new System.Windows.Forms.TextBox();
            this.clearBtn = new System.Windows.Forms.Button();
            this.createBtn = new System.Windows.Forms.Button();
            this.rapEntryBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "RAPs folder 2:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "RAPs folder 1:";
            // 
            // rapPath2Btn
            // 
            this.rapPath2Btn.Location = new System.Drawing.Point(408, 88);
            this.rapPath2Btn.Name = "rapPath2Btn";
            this.rapPath2Btn.Size = new System.Drawing.Size(75, 23);
            this.rapPath2Btn.TabIndex = 7;
            this.rapPath2Btn.Text = "Browse &2...";
            this.rapPath2Btn.UseVisualStyleBackColor = true;
            this.rapPath2Btn.Click += new System.EventHandler(this.rapPath2Btn_Click);
            // 
            // rapPath1Btn
            // 
            this.rapPath1Btn.Location = new System.Drawing.Point(408, 56);
            this.rapPath1Btn.Name = "rapPath1Btn";
            this.rapPath1Btn.Size = new System.Drawing.Size(75, 23);
            this.rapPath1Btn.TabIndex = 6;
            this.rapPath1Btn.Text = "Browse &1...";
            this.rapPath1Btn.UseVisualStyleBackColor = true;
            this.rapPath1Btn.Click += new System.EventHandler(this.rapPath1Btn_Click);
            // 
            // rapPath2
            // 
            this.rapPath2.Enabled = false;
            this.rapPath2.Location = new System.Drawing.Point(88, 88);
            this.rapPath2.Name = "rapPath2";
            this.rapPath2.ReadOnly = true;
            this.rapPath2.Size = new System.Drawing.Size(312, 20);
            this.rapPath2.TabIndex = 5;
            this.rapPath2.TabStop = false;
            // 
            // rapPath1
            // 
            this.rapPath1.Enabled = false;
            this.rapPath1.Location = new System.Drawing.Point(88, 56);
            this.rapPath1.Name = "rapPath1";
            this.rapPath1.ReadOnly = true;
            this.rapPath1.Size = new System.Drawing.Size(312, 20);
            this.rapPath1.TabIndex = 4;
            this.rapPath1.TabStop = false;
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(64, 24);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(48, 23);
            this.clearBtn.TabIndex = 3;
            this.clearBtn.Text = "C&lear";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // createBtn
            // 
            this.createBtn.Enabled = false;
            this.createBtn.Location = new System.Drawing.Point(8, 24);
            this.createBtn.Name = "createBtn";
            this.createBtn.Size = new System.Drawing.Size(48, 23);
            this.createBtn.TabIndex = 2;
            this.createBtn.Text = "&Create";
            this.createBtn.UseVisualStyleBackColor = true;
            this.createBtn.Click += new System.EventHandler(this.createBtn_Click);
            // 
            // rapEntryBox
            // 
            this.rapEntryBox.Location = new System.Drawing.Point(120, 8);
            this.rapEntryBox.Multiline = true;
            this.rapEntryBox.Name = "rapEntryBox";
            this.rapEntryBox.Size = new System.Drawing.Size(360, 32);
            this.rapEntryBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "RAP name and value:";
            // 
            // mWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 120);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rapPath2Btn);
            this.Controls.Add(this.rapPath1Btn);
            this.Controls.Add(this.rapPath2);
            this.Controls.Add(this.rapPath1);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.createBtn);
            this.Controls.Add(this.rapEntryBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "mWindow";
            this.Text = "RAP Creator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button createBtn;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.TextBox rapPath1;
        private System.Windows.Forms.TextBox rapPath2;
        private System.Windows.Forms.Button rapPath1Btn;
        private System.Windows.Forms.Button rapPath2Btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox rapEntryBox;
    }
}

